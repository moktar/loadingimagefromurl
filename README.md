LoadingImageFromURL
===================

loading an image into imageview from URL (HTTP)

I am writing this demo as lot of people are asking me about loading an image into imageview. Just by adding url to ImageView won’t load directly. Instead we need to store the image into cache (temporary location) and attach to ImageView. This can be achieved just by calling couple of lines and don’t forget to add required classes to your project.

